﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets
{
    public class Edge
    {
        public Node[] ComposeNodes;
        public int Weight;

        public Edge(Node Start, Node End, int Weight)
        {
            ComposeNodes = new Node[2];
            ComposeNodes[0] = Start;
            ComposeNodes[1] = End;
            this.Weight = Weight;

        }
    }
}
