﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour
{
    public List<Node> AdjacentNodes;
    

    public void CheckAdjacentNode(Vector3 dir)
    {

     

        Debug.Log("Origin " + this.gameObject.transform.position + "Dest " + dir);
        if (Physics.Linecast(this.gameObject.transform.position, dir, out var hit))
        {

            Node aux = hit.collider.GetComponent<Node>();

            if (aux == null)
            {
                return;

            }
            AdjacentNodes.Add(aux);
            if(!aux.AdjacentNodes.Contains(this))
                aux.AdjacentNodes.Add(this);
        }

    }
    private void OnDrawGizmos()
    {
        for (int i = 0; i < AdjacentNodes.Count; i++)
        {
            Gizmos.DrawLine(transform.position, AdjacentNodes[i].transform.position);
            Gizmos.color = Color.red;

        }





    }


}
