﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    public float Grain = 1.0f;
    public int Size = 10;
    public Node Node;
    public Material Walk, DontWalk;
    public List<Node> GraphNodes;
   

  

    public void Awake()
    {
        GenerateGraph();
    }

    public void GenerateGraph()
    {
        RaycastHit ray;

        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                if (Physics.Raycast(this.transform.position + new Vector3(i * Grain, 0, j * Grain),
                                    Vector3.down, out ray, 100.0f))
                {
                    Node aux = GameObject.Instantiate(Node, ray.point, Quaternion.identity) as Node;




                    if (ray.collider.tag == "Walk")
                    {
                        aux.GetComponent<Renderer>().material = Walk;
                        aux.CheckAdjacentNode(new Vector3((ray.point.x - 1), ray.point.y, ray.point.z + 1));
                        aux.CheckAdjacentNode(new Vector3((ray.point.x + 1), ray.point.y, ray.point.z + 1));
                        aux.CheckAdjacentNode(new Vector3((ray.point.x    ), ray.point.y, ray.point.z + 1));

                        aux.CheckAdjacentNode(new Vector3((ray.point.x + 1), ray.point.y, ray.point.z));

                        aux.CheckAdjacentNode(new Vector3((ray.point.x - 1), ray.point.y, ray.point.z));

                        aux.CheckAdjacentNode(new Vector3((ray.point.x + 1), ray.point.y, ray.point.z - 1));
                        aux.CheckAdjacentNode(new Vector3((ray.point.x - 1), ray.point.y, ray.point.z - 1));
                        aux.CheckAdjacentNode(new Vector3((ray.point.x), ray.point.y, ray.point.z - 1));
                        GraphNodes.Add(aux);

                    }
                    else
                        aux.GetComponent<Renderer>().material = DontWalk;
                }
            }
        }
        //TODO generate a ADJACENNT NODES for hold the graph information
 



    }








}




